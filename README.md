```bash
# install dependencies
npm install

# serve with hot reload at localhost:8888
npm start

# 本地构建
npm build 

# 版本构建
npm build:webpack
```

## common 公共方法（web only）
### Storage
#### Set
##### setStore(key, value[, type])
```js
import { setStore } from 'uJs'

// set local
setStore('key', 'value')
setStore('okey', { value: 'abc })

// set session
setStore('key', 'value', 'session')
setStore('okey', { value: 'abc }, 'session')
```

#### Get
##### getStore(key[, type])
```js
import { getStore } from 'uJs'

// get local
getStore('key')  // -> value
getStore('okey')  // -> { value: 'abc }

// get session
getStore('key', 'session')  // -> value
getStore('okey', 'session')  // -> { value: 'abc }
```
#### Remove
##### removeStore(key[, type])
```js
import { removeStore } from 'uJs'

// remove
removeStore('key')
removeStore('key', 'session')
```
