/**
 * 开发模式下的webpack配置
 * 在整个项目开发过程中，几乎99%的时间都是在这个模式下进行的
 * 注意。两种模式的配置有较大差异！！
 */

const path = require('path');
const webpack = require('webpack');
const HtmlWebpackPlugin = require('html-webpack-plugin')
const ExtractTextPlugin = require('extract-text-webpack-plugin');
const precss = require('precss')
const autoprefixer = require('autoprefixer')
const rucksackCss = require('rucksack-css')
const px2rem = require('postcss-pxtorem');
const px2remOpts = {
  rootValue: 50,
  propWhiteList: [],
};
const defaultSettings = {
  publicPath: 'src/',
}

export default {
  debug: true,
  devtool: 'cheap-module-eval-source-map', // more info:https://webpack.github.io/docs/build-performance.html#sourcemaps and https://webpack.github.io/docs/configuration.html#devtool
  noInfo: true, // set to false to see a list of every file being bundled.
  entry: [
    './src/webpack-public-path',  // 服务器静态资源路径配置，保证首先载入
    'react-hot-loader/patch',
    'webpack-hot-middleware/client?reload=true',
    path.resolve(__dirname, 'src/index.js')
    // 'index.js'
  ],
  target: 'web', // necessary per https://webpack.github.io/docs/testing.html#compile-and-test
  output: {
    path: `${__dirname}/src`, // Note: Physical files are only output by the production build task `npm run build`.
    publicPath: '/',
    filename: 'bundle.js'
  },
  // eslint: {
  //   configFile: './.eslintrc.js',
  // },
  plugins: [
    new webpack.DefinePlugin({
      'process.env.NODE_ENV': JSON.stringify('development'), // Tells React to build in either dev or prod modes. https://facebook.github.io/react/downloads.html (See bottom)
      __DEV__: true
    }),
    new webpack.HotModuleReplacementPlugin(),
    new webpack.NoErrorsPlugin(),
    new HtmlWebpackPlugin({     // Create HTML file that includes references to bundled CSS and JS.
      template: 'src/index.html',
      title: '开发模式',
      favicon:'./src/favicon.ico',
      minify: {
        removeComments: true,
        collapseWhitespace: true
      },
      hash:true,
      // 这样每次客户端页面就会根据这个hash来判断页面是否有必要刷新
      // 在项目后续过程中，经常需要做些改动更新什么的，一但有改动，客户端页面就会自动更新！
      inject: 'body'
    })
  ],
  resolve: {
    modulesDirectories: ['node_modules', path.join(__dirname, '../node_modules')],
    extensions: ['', '.web.js', '.js', '.json'],

    // 路径别名, 懒癌福音
    alias:{
			app:path.resolve(__dirname,'src/js'),
			// 以前你可能这样引用 import { Nav } from '../../components'
			// 现在你可以这样引用 import { Nav } from 'app/components'

			style:path.resolve(__dirname,'src/styles'),
			// 以前你可能这样引用 import "../../../styles/mixins.scss"
			// 现在你可以这样引用 import "style/mixins.scss"

      uJs:path.resolve(__dirname, 'src/js/utils/common')
      // 以前你可能这样引用 import { getStore } from "../../../utils/common/index.js"
			// 现在你可以这样引用 import { getStore } from 'uJs

			// 注意：别名只能在.js文件中使用。
    }
  },
  module: {
    rules: [
      {
        test: /\.scss$/,
        //exclude: /node_modules/,
        include: path.resolve(__dirname, 'src/styles'),
        use: ExtractTextPlugin.extract({
          //publicPath: '../../',
          fallback: 'style-loader',
          //resolve-url-loader may be chained before sass-loader if necessary
          use: [
            {
              loader: "css-loader",
              options: {
                sourceMap: true,
                minimize: true,
              }
            },
            {
              loader: 'postcss-loader',
              options: { sourceMap: true }
            },
            {
              loader: "sass-loader",
              options: { sourceMap: true }
            }
          ]
        })
      },
      {
        test: /\.(css|scss)$/,
        exclude: /node_modules/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          //resolve-url-loader may be chained before sass-loader if necessary
          use: [
            {
              loader: "css-loader",
              options: { sourceMap: true }
            },
            {
              loader: 'postcss-loader',
              options: { sourceMap: true }
            },
            {
              loader: "sass-loader",
              options: { sourceMap: true }
            }
          ]
        })
      },
      {
        test: /\.scss$/,
        //exclude: /node_modules/,
        include: path.resolve(__dirname, 'src/components'),
        use: ExtractTextPlugin.extract({
          //publicPath: '../../',
          fallback: 'style-loader',
          //resolve-url-loader may be chained before sass-loader if necessary
          use: [
            {
              loader: "css-loader",
              options: {
                sourceMap: true,
                minimize: true,
                modules: true,
                importLoaders: 1,
                localIdentName: '[local]___[hash:base64:5]'
              }
            },
            {
              loader: 'postcss-loader',
              options: { sourceMap: true }
            },
            {
              loader: "sass-loader",
              options: { sourceMap: true }
            }
          ]
        })
      },
      {
        test: /\.(js|jsx)$/,
        exclude: /node_modules/,
        use: [
          'babel-loader',
          'eslint-loader'
        ]
      },
      {
        test: /\.(jpg|jpeg|png|svg|gif|bmp)/i,
        use: [
          `url-loader?limit=5000&name=${defaultSettings.publicPath}images/[hash:8].[name].[ext]`
        ],
      },
      {
        test: /\.css$/,
        exclude: path.resolve(__dirname, 'node_modules'),
        use: ['style-loader', 'css-loader', 'postcss-loader']
      },
    ]
  },
  postcss: ()=> [precss,autoprefixer,rucksackCss,px2rem(px2remOpts)]
};
