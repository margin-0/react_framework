
import React, { Component } from 'react';
import { connect } from "react-redux";
import { Nav, OrderAmount, Button } from "../index";
class ChooseCards extends Component {

  constructor(props) {
    super(props);
    this.state = {
        title: '选择银行卡',
        text: '确认'
    }
  }

  render() {

    console.log(this.props.state, '选择卡')
    let { order } = this.props; 
    let amountData = { orderAmount: order.orderAmount, creditLimitMax: order.creditLimitMax, creditLimitMin: order.creditLimitMin };

    console.log(this.props.order)
    return (
      <div>
          <Nav data={this.state}/>
          <OrderAmount data={amountData} />
          <Button data={this.state}/>
      </div>
    )
  }
}

export default connect(state => {
  return {
    order: state.order
  };
})(ChooseCards);