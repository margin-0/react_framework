
import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { Button } from '../index.js';
import Numeral from 'numeral';
import styles from './index.scss';

class DownPayment extends Component {
  constructor(props) {
    super(props);
    this.state = {
      val: '',
      canPay: true,
      orderAmount: localStorage.getItem("orderAmount"),
      creditLimit: localStorage.getItem("creditLimit"),
      initialAmount: localStorage.getItem("initialAmount")
    };
  }

  componentDidMount() {
  }

  handleChange(e) {
    console.log("---v---", e);
    let val = e.target.value;
    let { orderAmount, creditLimit } = this.state;
    let payMax = (orderAmount >= creditLimit) ? creditLimit : orderAmount;
    this.setState({ val: e.target.value });
    if (Number(val) < 0 || Number(val) > Number(payMax)) {
      this.setState({ canPay: false });
    } else {
      this.setState({ canPay: true });
    }
  }

  render() {
    console.log(this.props);

  
    const { val, canPay } = this.state;
    let props = { title: "首付", text: "确认", canGo: canPay, router: 'downpay' };
    // 订单总额
    let orderAmount = Numeral(localStorage.getItem("orderAmount")).format(
      '0.00'
    );
    // 可用额度
    let creditLimit = Numeral(localStorage.getItem("creditLimit")).format(
      "0.00"
    );
    // 初始首付
    let initialAmount = Numeral(localStorage.getItem("initialAmount")).format(
      "0.00"
    );
    // 最多支付
    let payMax = orderAmount - creditLimit >= 0 ? creditLimit : orderAmount;
   
    return (
      <div>
        <div className={styles.info}>
          您的订单总额￥{orderAmount}，可用额度￥{creditLimit}，本单首付金额最少￥{
            initialAmount
          }元，最多￥{payMax}。
        </div>
        <div className={styles.cont}>
          <div className={styles.text}>首付金额</div>
          <div className={styles.area}>
            <span>￥</span>
            <input
              placeholder='0'
              onChange={e => this.handleChange(e)}
              type="number"
              value={val}
            />
            <span className={styles.write}>可修改</span>
          </div>
        </div>
        <Button data={props} />
      </div>
    );
  }
}

export default connect( state => {
  return {
    order: state.order
  }
} )(DownPayment)
