import React, { Component } from 'react'

import styles from './index.scss';

class StagingList extends Component {
    constructor(props) {
        super(props)
        this.state = {
            choose: 0
        }

        this.handlerClick = this.handlerClick.bind(this);
    }
    componentDidMount() {
        let id = 0;
        let { data } = this.props;
        data.forEach( item => {
            if(item.choose) id = item.id
        });
        this.setState({
            choose: id
        });
    }

    handlerClick(e) {
        this.setState({
            choose: e
        })
    }

    render() {
        // console.log(this.props, '??????????');
        let { data } = this.props
        return (
            <ul className={styles.installmentsUl}>
                {
                    data && data.map( (item, index) => {
                        let btnClass = (index == this.state.choose ) ? styles.installmentsBtn + ' ' + styles.installmentsBtnActive : styles.installmentsBtn;
                        return (
                            <li key={index}>
                                <div
                                    className={ btnClass }
                                    onClick={ () => this.handlerClick(index) } ></div>
                                <p>
                                    <span className={styles.installmentsListPrice}>￥{item.periodAmount}</span>&nbsp;&nbsp;&nbsp;
                                    <span className={styles.installmentsListDate}>{item.periodTotal}期</span></p>
                                <p className={styles.installmentsListInfo}>含服务费￥{item.serviceCharge}，利息￥{item.interest}</p>
                            </li>
                        )
                    } )
                }


            </ul>
        )
    } 
}
export default StagingList;
