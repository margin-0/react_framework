
import React, { Component } from 'react';
import { connect } from "react-redux";
import { Link, withRouter } from "react-router-dom";
import { WhiteSpace, List } from "antd-mobile";
import { Nav, OrderAmount, Stages, Button } from "../index";
import { getOrder } from "../../action/action";

class ChooseCards extends Component {

  constructor(props) {
    super(props);
    this.state = {
      title: '收银台'
    }
    this.handleClick = this.handleClick.bind(this)
  }

  componentDidMount() {
    this.props.dispatch(getOrder());
  }

  handleClick() {
    console.log(this.props);
    // this.props.history.push('/paymentstatus');
  }

  render() {
    const { Item } = List;
    const { Brief } = Item;
    let { order } = this.props;
    let isTrue = JSON.stringify(order) !== '{}';
    let props = { title: '收银台', text: '确认订单', canGo: true, router: 'home' };
    let amountData = { orderAmount: order.orderAmount, creditLimit: order.creditLimit, initialAmount: order.initialAmount };
    
    // if(!Common.id) {
    // Common.id = order.orderId;
    localStorage.setItem("orderId", order.orderId);
    localStorage.setItem("orderAmount", order.orderAmount);
    localStorage.setItem("creditLimit", order.creditLimit);
    localStorage.setItem("initialAmount", order.initialAmount);
    // }
    console.log(order, "id2", amountData , isTrue);
    let stagesShow = order.canUseCredit ? <Stages data={order.repayPlanList} /> : <div />;

    return (
      <div>
        <article>
          <Nav data={this.state} />
        </article>
        {isTrue && <main>
          <OrderAmount data={amountData} />
          <WhiteSpace />

          <List>
            <Item arrow="horizontal" multipleLine onClick={() => { }}>
              信用支付 <Brief>
                可用额度￥ {amountData.creditLimit}
              </Brief>
            </Item>
          </List>
          <WhiteSpace />
          <Link to="./downpay">
            <List>
              <Item extra={amountData.initialAmount} arrow="horizontal" onClick={() => { }}>
                首付
                </Item>
            </List>
          </Link>
          {/* {stagesShow} */}
          <Button data={props} fun={this.handleClick}/>
          {/* <Link to='paymentstatus'>
            <Button data={props} />
          </Link> */}
        </main>}
      </div>
    )
  }
}

const Choose = withRouter(ChooseCards);

export default connect(state => {
  return {
    order: state.order
  };
})(Choose);