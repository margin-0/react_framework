import React, { Component } from 'react';
import { Toast } from "antd-mobile";
import styles from './index.scss';
import { Link } from 'react-router-dom';

class Button extends Component {
  constructor(props) {
    super(props);
    this.state = {
      text: this.props.data.text
    };
  }

  render() {
    let { text } = this.state;
    console.log(this.props.fun)
    // function showToast() {
    //   Toast.info('金额不符合要求', 2);
    // }
    // let { text, canGo, router } = this.props.data;

    // function showButton() {
    //   let button;
    //   switch (router) {
    //     case 'downpay':
    //       if (canGo) {
    //         button = (
    //           <Link to='home'>
    //             <div className={styles.confirmPayment}> {text}</div >
    //           </Link>
    //         )
    //       } else {
    //         button = (
    //           <div className={styles.confirmPayment} onClick={showToast} > {text}</div >
    //         )
    //       }
    //       break;
    //       case 'home':
    //         if (canGo) {
    //           button = (
    //             <Link to='paymentstatus'>
    //               <div className={styles.confirmPayment}> {text}</div >
    //             </Link>
    //           )
    //         } else {
    //           button = (
    //             <div className={styles.confirmPayment} onClick={showToast} > {text}</div >
    //           )
    //         }
    //       break
    //   }
    //   return button
    // }

    return (
      <React.Fragment>
        <div className={styles.confirmPayment} onClick={() => this.props.fun() }> {text}</div >
      </React.Fragment>
    );
  }
}

export default Button
