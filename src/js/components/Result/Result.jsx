import React, { Component } from "react";

import styles from "./index.scss";

class Result extends Component {
    render() {
        // let {
        //     status,
        //     cash,
        //     type,
        //     name,
        //     title,
        //     creditType,
        //     fee,
        //     productNo,
        //     errorMsg
        // } = this.state;
        //
        // let modalStyle = {
        //     width: '90%'
        // };
        //
        // let formatCash = '-';
        //
        // if (cash !== '-') {
        //     formatCash = `${Numeral(cash).divide(100).format('0, 0.00')}元`;
        // }
        //
        // let result = null;
        // let feeTitle = '支付中, 请耐心等待';
        // if (status === 'success') {
        //     feeTitle = `恭喜您成为${CONFIGS.memberJson[productNo]}`;
        // } else if (status === 'failed') {
        //     feeTitle = '支付失败';
        // }
        //
        // if (cash !== '-') {
        //     formatCash = `${Numeral(cash).divide(100).format('0, 0.00')}元`;
        //     result = (<div className={styles.root}>
        //         <div className={`${styles.resultStatus} ${styles[status]}`}></div>
        //         <div className={styles.resultTitle}>{title}</div>
        //         {type === 'f' && <div className={`${styles.resultCash} number`}>{feeTitle}</div>}
        //         {type !== 'f' && <div className={`${styles.resultCash} number`}>{formatCash}</div>}
        //         {(type === 'p' || type === 'r' || type === 'e') && fee && <div className={styles.resultMessageText}>{`（含支付通道费${fee}元，代资金存管机构收取）`}</div>}
        //         <div className={styles.resultMessage}>
        //             {(type === 's' && creditType) && <span className={styles.resultMessageText}>{CONFIGS.resultDetail[type][creditType][status]}</span>}
        //             {(type === 'p' || type === 'r') && <span className={styles.resultMessageText}>{CONFIGS.resultDetail[type][status]}</span>}
        //             {(type === 'r') && (status !== 'failed') && <span className={styles.resultMessageWarning} onClick={this.showModal('modal')}></span>}
        //             {(type === 'f') && (status === 'default') && <span className={styles.resultMessageText}>{CONFIGS.resultDetail[type][status]}</span>}
        //             {(type === 'f') && (status === 'failed') && <span className={styles.resultMessageText}>{errorMsg}</span>}
        //         </div>
        //         {(this.state.showPhone !== 1) && (cash !== '-') && (<Phone/>)}
        //         <Modal className="crf-result-modal" style={modalStyle} title="温馨提示" transparent="transparent" maskClosable={false} visible={this.state.modal} onClose={this.onClose('modal')} platform="ios">
        //             <div className="crf-result-modal-body">
        //                 <p>按时还款以还款提交成功时间为准, 因系统或银行原因导致的延迟, 不属于逾期, 请勿担心</p>
        //                 <p>
        //                     <button className="normal-btn" onClick={this.onClose('modal')}>知道了</button>
        //                 </p>
        //             </div>
        //         </Modal>
        //     </div>)
        // } else {
        //     result = (<div></div>);
        // }
        let title = "支付成功";
        let feeTitle =
            "预10分钟内出结果，<br/>部分银行可能延迟, 以实际情况为准。";
        let formatCash = "-";
        let fee = "520";
        console.log(styles);
        return (
            <div className={styles.root}>
                <div className={styles.state}>
                    <div
                        className={`${styles.resultStatus} ${styles.success}`}
                    />
                    <div className={styles.resultTitle}>{title}</div>
                    <div className={`${styles.resultCash} number`}>
                        预10分钟内出结果，<br />部分银行可能延迟,
                        以实际情况为准。
                    </div>
                </div>
            </div>
        );
    }
}

export default Result;
