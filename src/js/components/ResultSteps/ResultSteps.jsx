import React, { Component } from "react";

import { Steps } from "antd-mobile";
const { Step } = Steps;

import styles from "./index.scss";

class ResultSteps extends Component {
    constructor(props, context) {
        super(props, context);
        this.state = {
            status: props.data.status,
            dataList: props.data.dataList,
            loanClassName: props.loanClassName
        };
    }
    render() {
        const { status, dataList, loanClassName } = this.state;

        const step = (item, index) => {
            let styleName = "";

            if (index === dataList.length - 1 && status === 2) {
                styleName = "finish";
            } else if (index === dataList.length - 1 && status === 1) {
                styleName = "error";
            }
            return (
                <Step
                    key={index}
                    className={styleName}
                    title={item.trace_content}
                    description={item.trace_time}
                />
            );
        };
        const realSteps = (
            <Steps current={dataList.length - 1} className={loanClassName}>
                {dataList.map(step)}
            </Steps>
        );

        return (
            <div className={styles.resultContent}>
                {/* {realSteps}*/}
                <div>{realSteps}</div>
            </div>
        );
    }
}

export default ResultSteps;
