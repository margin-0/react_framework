
import React, { Component } from 'react';
import { WhiteSpace } from 'antd-mobile';

import { StagingList } from '../index';
const styles = require('./index.scss');

class Stages extends Component {

    constructor(props) {
        super(props);
        this.state = {
            stages: [
                {
                    id: 0,
                    price: '1997.01',
                    period:  '3',
                    serviceCharge: '5.00',
                    interest: '2.00',
                    choose: false
                },
                {
                    id: 1,
                    price: '2004.01',
                    period:  '6',
                    serviceCharge: '10',
                    interest: '4',
                    choose: true
                },
                {
                    id: 2,
                    price: '1007.83',
                    period:  '9',
                    serviceCharge: '11.50',
                    interest: '4',
                    choose: false
                },
                {
                    id: 3,
                    price: '1997.01',
                    period:  '12',
                    serviceCharge: '15.50',
                    interest: '4',
                    choose: false
                }
            ],
            stages2: [...this.props.data]
        }

        // this.onHandlerClick = this.onHandlerClick.bind(this);
    }

    // onHandlerClick(e) {
    //     let { stages } = this.state;
    //
    //     stages.forEach( item => {
    //         item.choose = ( item.id == e ) ? true : false;
    //     });
    //
    //     this.setState({
    //         stages: [].concat(stages)
    //     })
    // }

    componentDidMount() {
      // this.
    }

    render() {
        console.log(this.state.stages2,'stages2');
        console.log(styles);

        let { stages2 } = this.state;
        return (
            <div>
                <WhiteSpace />
                <div className={styles.installmentsBox}>
                    <p className={styles.installmentsTitle}>
                        <span className={styles.installmentsText}>分期方式</span>
                        {/* <span className={styles.installmentsPrice}>
                            应还金额<span className={styles.installmentsPriceSpan}>￥2004.00</span>
                        </span> */}
                    </p>
                    <StagingList
                        data={stages2}
                        // Click={this.onHandlerClick}
                     />
                </div>
            </div>
        )
    }
}


export default Stages;
