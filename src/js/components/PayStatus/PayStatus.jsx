import React, { Component } from 'react';
import Numeral from 'numeral';
// 回头在研究研究 滑动效果
// import ReactPullToRefresh from "react-pull-to-refresh";
import { Modal } from "antd-mobile";
import styles from './index.scss';

class PayStatus extends Component {

  constructor(props) {
    super(props);
    this.state = {
      status: '支付中',
      cash: '5000',
      type: 'cash',
      name: 'jeck',
      title: '系统处理中',
      creditType: 'creditType',
      fee: 'fee',
      productNo: 'productNo',
      errorMsg: 'errorMsg'
    }
    
    this.handleRefresh = this.handleRefresh.bind(this);
  }

  showModal = key => (e) => {
    // 现象：如果弹出的弹框上的 x 按钮的位置、和手指点击 button 时所在的位置「重叠」起来，
    // 会触发 x 按钮的点击事件而导致关闭弹框 (注：弹框上的取消/确定等按钮遇到同样情况也会如此)
    e.preventDefault(); // 修复 Android 上点击穿透
    this.setState({
      [key]: true,
    });
  }

  onClose = key => () => {
    this.setState({
      [key]: false,
    });
  }

  handleRefresh(resolve, reject) {
    // do some async code here
    if (success) {
      resolve();
    } else {
      reject();
    }
  }

  render() {

    let { status, cash, type, name, title, creditType, fee, productNo, errorMsg } = this.state;

    let modalStyle = { width: '90%' };
    let formatCash = '-';
    if (cash !== '-') {
      formatCash = `${Numeral(cash).divide(100).format('0, 0.00')}元`;
    }
    let result = null;
    let feeTitle = '支付中, 请耐心等待';
    if (status === 'success') {
      feeTitle = `恭喜您成为`;
    } else if (status === 'failed') {
      feeTitle = '支付失败';
    }

    if (cash !== '-') {
      formatCash = `${Numeral(cash).divide(100).format('0, 0.00')}元`;
      result = (
        <div className={styles.root}>
          <div className={`${styles.resultStatus} ${styles[status]}`}></div>
          <div className={styles.resultTitle}>{title}</div>
          {type === 'f' &&
            <div className={`${styles.resultCash} number`}>{feeTitle}</div>
          }
          {type !== 'f' &&
            <div className={`${styles.resultCash} number`}>{formatCash}</div>
          }
          {(type === 'p' || type === 'r' || type === 'e') && fee &&
            <div className={styles.resultMessageText}>{`（含支付通道费${fee}元，代资金存管机构收取）`}</div>
          }
          <div className={styles.resultMessage}>
            {(type === 's' && creditType) &&
              <span className={styles.resultMessageText}>{CONFIGS.resultDetail[type][creditType][status]}</span>
            }
            {(type === 'p' || type === 'r') &&
              <span className={styles.resultMessageText}>{CONFIGS.resultDetail[type][status]}</span>
            }
            {(type === 'r') && (status !== 'failed') &&
              <span className={styles.resultMessageWarning} onClick={this.showModal('modal')}></span>
            }
            {(type === 'f') && (status === 'default') &&
              <span className={styles.resultMessageText}>{CONFIGS.resultDetail[type][status]}</span>
            }
            {(type === 'f') && (status === 'failed') &&
              <span className={styles.resultMessageText}>{errorMsg}</span>
            }
          </div>
          {/* {(this.state.showPhone !== 1) && (cash !== '-') && (
            <Phone />
          )} */}
          <Modal
            className="crf-result-modal"
            style={modalStyle}
            title="温馨提示"
            transparent
            maskClosable={false}
            visible={this.state.modal}
            onClose={this.onClose('modal')}
            platform="ios"
          >
            <div className="crf-result-modal-body">
              <p>按时还款以还款提交成功时间为准, 因系统或银行原因导致的延迟, 不属于逾期, 请勿担心</p>
              <p><button className="normal-btn" onClick={this.onClose('modal')}>知道了</button></p>
            </div>
          </Modal>
        </div>
      )
    } else {
      result = (
        <div></div>
      );
    }
    return(
      <div>
        {/* <ReactPullToRefresh
          onRefresh={this.handleRefresh}
          className="your-own-class-if-you-want"
          style={{
            textAlign: 'center'
          }}
        > */}
          <span className="genericon genericon-next"></span>
          <div className="loading">
            <span className="loading-ptr-1"></span>
            <span className="loading-ptr-2"></span>
            <span className="loading-ptr-3"></span>
          </div>
          {result}
        {/* </ReactPullToRefresh> */}
        
      </div>
    )
  }
}

export default PayStatus;