
import React, { Component } from 'react';
import { WhiteSpace, List } from 'antd-mobile';
import numeral from 'numeral';

import styles from './index.scss';
// const styles = require('./index.scss');


class OrderAmount extends Component {

    constructor(props) {
        super(props);
        this.state = {
            orderAmount: this.props.data.orderAmount,
            creditLimitMax: this.props.data.creditLimitMax
        }
    }

    componentDidMount () {
        console.log(this.props.data);
        console.log('=================');
    }

    render() {

        const { Item } = List;
        const { Brief } = Item;
        const Amount = numeral(this.state.orderAmount)._value;

        let AmountSpan = null;
        AmountSpan =  Amount ? <span className={styles.aa}> ￥ {numeral(Amount)._value} </span> : <span></span>;

        return (
            <div className={styles.cont}>
                <List>
                    <Item extra={ AmountSpan }>订单金额</Item>
                </List>
                

            </div>
        )
    }
}

export default OrderAmount;
