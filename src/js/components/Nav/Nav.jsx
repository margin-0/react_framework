
import React, { Component } from 'react';
import { Toast } from 'antd-mobile';

import styles from "./index.scss";


class Nav extends Component {
    constructor(props) {
        super(props);
        this.state = {
            title: this.props.data
        }

        this.handleBack = this.handleBack.bind(this);

    }

    handleBack() {
        console.log('返回');
        history.back();
    }

    render() {
        console.log(1111);
        let { title } = this.state.title;
        console.log(this.props, '====');
        let leftEle = null;
        leftEle = <span className={styles.navbarLeftIcon}><button className={styles.trsBtn} onClick={this.handleBack}></button></span>;
        let rightEle = null;
        return (
            <div>
              <nav className={styles.root}>
                <div className={styles.navbarLeft}>
                  {leftEle}
                </div>
                <div className={styles.navbarTitle}>{title}</div>
                <div className={styles.navbarRight}>
                  {rightEle}
                </div>
              </nav>
              <div className={styles.fh}></div>
            </div>
        )
    }
}

export default Nav;
