export default ( order = {}, action ) => {

    switch ( action.type ) {
        case 'GET_INIT':
            return {...action.data}
        case 'UPDATA_ORDER':
            return {...action.data}
        default:
            return order
    }
}
