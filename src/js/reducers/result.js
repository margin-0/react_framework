import { mergeState } from 'uJs'

const initialState = {
  title: '结果页面',
}

const result = (state = initialState, action) => {
  switch (action.type) {
    case 'RESULT_DATA':
    case 'RESULT_STATUS':
      return mergeState(state, action)
    default:
      return state
  }
}

export default result