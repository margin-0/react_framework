import { combineReducers } from 'redux'
// 引入reduces
import order from './order'
import pay   from './pay'
import result from './result'

export default combineReducers({
  order,
  pay,
  result,
})