/**
 * 这里定义所有的 actions 类型
 */

// 获取用户基础状态
export const GET_INIT = '/pcashier/cashier/init';

// 获取订单初始信息
export const GET_ORDER = '/pcashier/cashier/index';

// 获取分期列表
export const GET_PLANLIST = '/pcashier/cashier/planList';

// 支付
export const PAY = '/pcashier/cashier/pay';

//  查询支付状态
export const QUERY_PAY_STATUS = '/pcashier/cashier/queryStatus';


// 预绑卡
export const ADD_CARDS = '/pcashier/bank/bind/add';

// 取消绑卡
export const CANCEL_CARDS = '/pcashier/bank/bind/cancel';

// 确定绑卡
export const CONFIRM_CARD = '/pcashier/bank/bind/conform';

// 查询用户银行卡
export const CHECK_USER_CARDS = '/pcashier/bank/cards';

// 查询卡列表
export const CHECK_CARDS = '/pcashier/bank/json';
