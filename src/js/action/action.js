
import axios from 'axios';

import {
  GET_INIT,
  GET_ORDER,
  QUERY_PAY_STATUS
} from './actionType';

export const getInit = () => {

    return dispatch => {
        axios.post(GET_INIT)
            .then( ( {data} ) => {
                dispatch( {
                    type: 'GET_INIT',
                    data: data
                } )
            } )
    }
};

export const getOrder = () => {

  return dispatch => {
    axios(GET_ORDER)
      .then( ({data}) => {
        dispatch( {
          type: 'UPDATA_ORDER',
          data: data
        } )
      } )
  }
}

export const queryPayStatus = (orderId) => {
  return dispatch => {
    axios(`${QUERY_PAY_STATUS}?orderId=${orderId}`)
      .then( (data) => {
        dispatch( {
          type: 'QUERY_PAY_STATUS',
          data: data
        } )
      } )
  }
}