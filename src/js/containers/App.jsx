import React, { Component } from 'react'

export default class App extends Component {
    render() {
        const { children } = this.props;
        console.log('------App---', children)
        return children 
    }
}