
import React,{ Component } from 'react';
import { connect } from 'react-redux';
import { Cashier, ChooseCards } from "../components";

import { getInit } from "../action/action";

class Home extends Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {
        if(JSON.stringify(this.props.order) == '{}') {
          this.props.dispatch( getInit() )
        }
        // browserHistory.push(`/downpayment/`)
    }

    render() {

      let { order } = this.props;
      
      let isOrder = JSON.stringify(order)
      function isTrue(){
        if (order.canUseCredit && order.initialAmount >= -1) {
          return true
        } else {
          return false
        }
      };
        return (
            <div>
              {
                isOrder != '{}' && (
                isTrue() ? <Cashier /> : <ChooseCards />
                )
              }
                
            </div>
        )
    }
}

export default connect( state => {
    return {
        order: state.order,
        
    }
} )(Home);
