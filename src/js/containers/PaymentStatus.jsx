
import React, { Component } from 'react';
import { Nav, PayStatus } from "../components";
import { connect } from 'react-redux';
import { queryPayStatus } from '../action/action';

class PaymentStatus extends Component {

  constructor(props) {
    super(props);
    this.state = {
      orderId: ''
    }
  }

  componentDidMount() {
    let orderId = localStorage.getItem("orderId")
    this.props.dispatch(queryPayStatus(orderId) )
  }

  render() {

    let props = { title: '支付状态' };

    return (
      <div>
          <Nav data={props}/>
          <PayStatus />
      </div>
    )
  }
}

export default connect( state => {
  return {
    order: state.order,
    payStates: state.payStates
  }
} )(PaymentStatus);
