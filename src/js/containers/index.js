export { default as App } from './App.jsx';

export { default as BindCard } from './BindCard.jsx';

export { default as Result } from './Result.jsx';

export { default as DownPay } from './DownPay.jsx';

export { default as Home } from './Home.jsx';

export { default as PaymentStatus } from './PaymentStatus.jsx';

export { default as DownPayment } from './DownPayment.jsx'