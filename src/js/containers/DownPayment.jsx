
import React, { Component } from 'react';
import { Nav } from 'app/components';

class DownPayment extends Component {

    render() {

        let props = { title: '首付' };

        return (
            <div>
                <article>
                    <Nav data={props}/>
                </article>
            </div>
        )
    }
}

export default DownPayment;
