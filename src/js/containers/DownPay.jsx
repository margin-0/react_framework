import React, { Component } from "react";
import { Nav, DownPayment, Button } from "../components";

class DownPay extends Component {
  render() {
    let props = { title: "首付", text: '确认' };

    return (
      <div>
        <article>
          <Nav data={props} />
        </article>
        <DownPayment />
      </div>
    );
  }
}

export default DownPay;
