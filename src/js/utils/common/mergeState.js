import { compose, merge, dissoc, clone } from 'ramda'

export default(
  (state, action) => compose(merge(state), dissoc('type'), clone)(action)
)