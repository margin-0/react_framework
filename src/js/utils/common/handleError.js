import { Toast } from 'antd-mobile';
import { setStore } from './setStore'

export default (
  (err, fail, loginCallback) => {
    if (err === 'timeout') {
      Toast.info('系统超时，请稍后再试');
    } else if (global.Common.isType('Function')(fail)) {
      fail();
    } else {
      let msg = err && err.body;
      let status = err && err.response && err.response.status;
      if (!loginCallback) {
        loginCallback = (data) => {
          if (data && data.access_token) {
            setStore('uid', data.access_token);
            setStore('linkToken', data.access_token);
          }
          location.reload();
        };
      }
      switch (status) {
        case 401:
          global.CRFLogin && global.CRFLogin.initialize(loginCallback);
          break;
        case 403:
          Toast.info('您没有权限做此操作，请返回重试！');
          break;
        case 404:
          Toast.info('资源没有找到，访问出错！');
          break;
        case 500:
        case 502:
        case 504:
          Toast.info('服务器错误，请稍后再试!');
          break;
        default:
          msg && msg.then(data => {
            Toast.info(data.message);
          });
      }
    }
  }
)