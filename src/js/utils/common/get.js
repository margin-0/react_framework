import axios from 'axios';
import handleError from './handleError';

axios.defaults.withCredentials = true;

export default (
  {
    async GET(options) {
      let { url, success, fail } = options;
      try {
        let axiosPromise = axios.get(url);
        let result = await axiosPromise;
        success(result);
      } catch (err) {
        handleError(err, fail);
      }
    }
  }
)