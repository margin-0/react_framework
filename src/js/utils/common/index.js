// state
export mergeState from './mergeState'

// storage
export setStore    from './setStore'
export getStore    from './getStore'
export removeStore from './removeStore'

// store
export store from './store'

// json
export parseJSON from './parseJSON'

// ajax--get,--post(send),--error
export get         from './get'
// expoat send        from './send'
export handleError from './handleError'

// ajax --axios
export api from './api'