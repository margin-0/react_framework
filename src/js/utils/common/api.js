// 引入axios
import axios from 'axios'
import { Toast } from 'antd-mobile';

var cancel = {}
var promiseArr = {}
const CancelToken = axios.CancelToken
// 请求拦截器
axios.interceptors.request.use(config => {
  // 发起请求时，取消掉当前正在进行的相同请求
  if (promiseArr[config.url]) {
    promiseArr[config.url]('操作取消')
    promiseArr[config.url] = cancel
  } else {
    promiseArr[config.url] = cancel
  }
  return config
}, error => {
  return Promise.reject(error)
})

// 响应拦截器即异常处理
axios.interceptors.response.use(response => {
  return response
}, err => {
  if (err === 'timeout') {
    Toast.info('系统超时，请稍后再试');
  } else {
    let msg = err && err.body;
    let status = err && err.response && err.response.status;
    if (!loginCallback) {
      loginCallback = (data) => {
        if (data && data.access_token) {
          setStore('uid', data.access_token);
          setStore('linkToken', data.access_token);
        }
        location.reload();
      };
    }
    switch (status) {
      case 401:
        global.CRFLogin && global.CRFLogin.initialize(loginCallback);
        break;
      case 403:
        Toast.info('您没有权限做此操作，请返回重试！');
        break;
      case 404:
        Toast.info('资源没有找到，访问出错！');
        break;
      case 500:
      case 502:
      case 504:
        Toast.info('服务器错误，请稍后再试!');
        break;
      default:
        msg && msg.then(data => {
          Toast.info(data.message);
        });
    }
  }
  return Promise.resolve(err.response)
})

axios.defaults.baseURL = ''
// 设置默认请求头
// axios.defaults.headers = {
//   // 'X-Requested-With': 'XMLHttpRequest'
// }
axios.defaults.timeout = 10000
axios.defaults.withCredentials = true;

export default {
  // get请求
  get(url, param, ) {
    return new Promise((resolve, reject) => {
      axios({
        method: 'get',
        url,
        params: param,
        // 数据请求取消操作，暂不使用（需求不需要）
        // cancelToken: new CancelToken(c => {
        //   cancel = c
        // })
      }).then(res => {
        resolve(res)
      }).catch(err => {
        reject(err)
      })
    })
  },
  // post请求
  post(url, param) {
    return new Promise((resolve, reject) => {
      axios({
        method: 'post',
        url,
        data: param,
        // cancelToken: new CancelToken(c => {
        //   cancel = c
        // })
      }).then(res => {
        resolve(res)
      }).catch(err => {
        reject(err)
      })
    })
  },
}
