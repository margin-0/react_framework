import { ifElse, compose, equals, curry, type } from 'ramda'
import stringifyJSON from './stringifyJSON'
import store from './store'

export default (
  (k, v, t = 'local') =>
  ifElse(
    compose(equals('String'), type),
    set(k, t),
    oset(k, t),
  )(v)
)

const set  = curry((k, t, v) => store[t].setItem(k, v))
const oset = curry((k, t, o) => set(k, t, stringifyJSON(o)))