import React, { Component } from 'react';
import { Switch, Route, Redirect, Router  } from 'react-router-dom';
import createHistory from 'history/createHashHistory';

const history = createHistory();
import { 
    App,
    BindCard,
    DownPay,
    Home,
    PaymentStatus,
    NotFoundPage,
    DownPayment,
    Result,
} from './containers'

console.log(Result);
const Routes = () => (
    <Router history={ history }>
        <App>
            <Switch>
                <Route path="/" exact component={Home} />
                <Route path="/bindcard" exact component={BindCard} />
                <Route path="/downpayment" exact component={DownPayment} />
                <Route path="/paymentstatus" exact component={PaymentStatus} />
                <Route path="/result" exact component={Result} />
            </Switch>
        </App>
    </Router>
)

export default Routes;
