// import React from 'react'
// // import 'react-fastclick'  // 这个需要放到react下方才行
// import ReactDOM from 'react-dom'
// import { AppContainer } from 'react-hot-loader'
// import Redbox from 'redbox-react'
// import { Provider } from 'react-redux';
// import store from './redux/store';
// import App from "./containers/app";

// const rootEl = document.getElementById('app');


// global.Common = require('./common');

// ReactDOM.render(
//   <AppContainer errorReporter={Redbox}>
//       <Provider store={store}>
//           <App />
//       </Provider>
//   </AppContainer>,
//   rootEl
// )

// if (module.hot) {
//   /**
//    * Warning from React Router, caused by react-hot-loader.
//    * The warning can be safely ignored, so filter it from the console.
//    * Otherwise you'll see it every time something changes.
//    * See https://github.com/gaearon/react-hot-loader/issues/298
//    */
//   const orgError = console.error; // eslint-disable-line no-console
//   console.error = (message) => { // eslint-disable-line no-console
//     if (message && message.indexOf('You cannot change <Router routes>;') === -1) {
//       // Log the error as normally
//       orgError.apply(console, [message]);
//     }
//   };

//   module.hot.accept('./containers', () => {
//     // If you use Webpack 2 in ES modules mode, you can
//     // use <App /> here rather than require() a <NextApp />.
//     const NextApp = require('./containers').default;
//     render(
//       <AppContainer errorReporter={Redbox}>
//         <Provider store={store}>
//             <NextApp />
//         </Provider>
//       </AppContainer>,
//       rootEl
//     )
//   });
// }

import React        from 'react'
import { render }   from 'react-dom'
import { createStore, applyMiddleware } from 'redux'
import { Provider } from 'react-redux'
import thunk        from 'redux-thunk'
import reducers     from './reducers'
import Routes       from './routes'

// 样式引入
import 'style/normalize.scss'
import 'style/mixins.scss'
import 'style/app.scss'
import 'style/antdStyleReset.scss'
import 'style/font.scss'
import 'style/animations.scss'
import 'style/reset.scss'

// 定义根组件
const Root = () => (
  <Provider store={createStore(reducers, applyMiddleware(thunk))}>
    <Routes />
  </Provider>
)
// render(<Root />, document.getElementById('app'))
export default Root
